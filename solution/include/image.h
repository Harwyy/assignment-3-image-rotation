#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image create_image(uint32_t w, uint32_t h);
struct image rotate(struct image source);
void free_memory(struct image source);

#endif
