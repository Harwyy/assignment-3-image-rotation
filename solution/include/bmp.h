#include  "image.h"
#include <malloc.h>
#include  <stdio.h>


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize; // размер файла
    uint32_t bfReserved; // 0
    uint32_t bOffBits; // смещение до поля данныхЮ обычно 54
    uint32_t biSize; // размер структуры данных
    uint32_t biWidth; // ширина в точках
    uint32_t biHeight; // высота в точках
    uint16_t biPlanes; // всегда должно быть 1
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage; // количество байт в поле данных. Обычно устанавливается в 0
    uint32_t biXPelsPerMeter; // горизонтальное разрешение
    uint32_t biYPelsPerMeter; // вертикальное разрешение
    uint32_t biClrUsed; // количество используемых цветов
    uint32_t biClrImportant; // количество существеных цветов
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    FILE_ERROR,
    ERROR
};

enum read_status from_bmp(FILE* in, struct image *image);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* out, struct image *image);
