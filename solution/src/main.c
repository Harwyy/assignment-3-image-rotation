#include "main.h"


int main(int argc, char **argv) {

    if (argc != 4){
        printf("The number of arguments should be 4");
        return 0;
    }

    FILE *source_file = fopen(argv[1], "rb");
    if (source_file == NULL) {
        printf("It was not possible to read data from the file");
        return 0;
    }
    FILE *transformed_file = fopen(argv[2], "wb");
    if (transformed_file == NULL) {
        printf("It was not possible to write data to a file");
        return 0;
    }



    if (from_bmp(source_file, &image) != READ_OK) {
        printf("Error reading data from a file");
        return 0;
    }

    uint16_t angle = atoi(argv[3]) < 0 ? atoi(argv[3])+360 : atoi(argv[3]);
    uint16_t count_of_rotate = (360 - (angle % 360)) / 90;
    size_t i;
    for (i = 0; i < count_of_rotate; i++){
        struct image new_image = rotate(image);
        free_memory(image);
        image = new_image;
    }


    if (to_bmp(transformed_file, &image) != WRITE_OK) {
        printf("Error writing data to a file");
        return 0;
    }

    printf("Success!");
    return 0;
}
