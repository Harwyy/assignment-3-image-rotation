#include "image.h"
#include "bmp.h"


struct image create_image(uint32_t w, uint32_t h){
    struct image img;
    img.height = h;
    img.width = w;
    img.data = malloc(sizeof(struct pixel) * w * h);
    return img;
}

void free_memory(struct image source){
    if (source.data == NULL){
        return;
    }
    free(source.data);
    source.data = NULL;    
}

struct image rotate(struct image source){
    struct image new_image = create_image(source.height, source.width);
    for (size_t i = 0; i < new_image.height; i++){
        for (size_t j = 0; j < new_image.width; j++){
            new_image.data[new_image.width * i + j] = source.data[source.width * (source.height - j - 1) + i];
        }
    }
    return new_image;
}


