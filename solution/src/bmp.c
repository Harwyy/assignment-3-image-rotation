#include "bmp.h"
#include "image.h"

#define BFtype 0x4d42;
#define BFreserved 0;
#define BIplanes 1;
#define BIsize 40;
#define BBcount 24;
#define BIX 2834;
#define BIY 2834;
#define BIclr 0;

static uint32_t padding_uint32_t(uint32_t width){
    if (width == 0){
        return 0;
    }
    return (4 - (width * sizeof (struct pixel) %4 )) % 4;
}

static void file_close_memory_free(FILE* file, struct image image){
    fclose(file);
    free_memory(image);
}

enum read_status from_bmp(FILE* in, struct image *image){

    if (!in){
        // fclose(in);
        // free_memory(*image);
        file_close_memory_free(in, *image);
        return FILE_ERROR;
    }
    struct bmp_header bmpHeader;
    if (sizeof (struct bmp_header) != fread(&bmpHeader, 1, sizeof (struct bmp_header), in)){
        // fclose(in);
        // free_memory(*image);
        file_close_memory_free(in, *image);
        return READ_INVALID_HEADER;
    }
    if (bmpHeader.bfType != 0x4d42){
        // fclose(in);
        // free_memory(*image);
        file_close_memory_free(in, *image);
        return READ_INVALID_SIGNATURE;
    }
    fseek(in, 0, SEEK_END);
    uint32_t filesize = ftell(in);
    fseek(in, sizeof (struct bmp_header), SEEK_SET);
    if (bmpHeader.bfileSize != filesize ||
        bmpHeader.bfReserved != 0       ||
        bmpHeader.biPlanes != 1         ||
        bmpHeader.biSize != 40          ||
        bmpHeader.bOffBits != 14 + bmpHeader.biSize ||
        bmpHeader.biWidth < 1           ||
        bmpHeader.biHeight < 1          ||
        bmpHeader.biBitCount != 24      ||
        bmpHeader.biCompression != 0){
        // fclose(in);
        // free_memory(*image);
        file_close_memory_free(in, *image);
        return READ_INVALID_BITS;
    }
    *image = create_image(bmpHeader.biWidth, bmpHeader.biHeight);
    uint32_t padding = padding_uint32_t(bmpHeader.biWidth);
    for (uint32_t i = 0; i < bmpHeader.biHeight; i++) {
        if (fread(image->data + i * bmpHeader.biWidth, sizeof(struct pixel), bmpHeader.biWidth, in) != bmpHeader.biWidth) {
            // fclose(in);
            // free_memory(*image);
            file_close_memory_free(in, *image);
            return ERROR;
        }
        // for (size_t j = 0; j < padding; j++){
        //     fseek(in, 1, SEEK_CUR);
        // }
        fseek(in, padding, SEEK_CUR);
    }
    fclose(in);
    return READ_OK;
}

static struct bmp_header create_bmp_header(struct image* image){
    struct bmp_header bmpHeader;
    bmpHeader.bfType = BFtype;
    uint32_t w = (3*image->width+3)&(-4);
    bmpHeader.biSizeImage = image->height * w;
    bmpHeader.bfileSize = 54 + bmpHeader.biSizeImage;
    bmpHeader.bfReserved = BFreserved;
    bmpHeader.biPlanes = BIplanes;
    bmpHeader.biSize = BIsize;
    bmpHeader.bOffBits = 14 + bmpHeader.biSize;
    bmpHeader.biWidth = image->width;
    bmpHeader.biHeight = image->height;
    bmpHeader.biBitCount = BBcount;
    bmpHeader.biCompression = 0;
    bmpHeader.biXPelsPerMeter = BIX;
    bmpHeader.biYPelsPerMeter = BIY;
    bmpHeader.biClrUsed = BIclr;
    bmpHeader.biClrImportant = BIclr;
    return bmpHeader;
}

enum write_status to_bmp(FILE* out, struct image *image){
    struct bmp_header bmpHeader = create_bmp_header(image);
    if (fwrite(&bmpHeader, 1, sizeof(struct bmp_header), out) != sizeof(struct bmp_header)){
        // fclose(out);
        // free_memory(*image);
        file_close_memory_free(out, *image);
        return WRITE_ERROR;
    }
    uint32_t padding = padding_uint32_t(bmpHeader.biWidth);
    for (uint32_t i = 0; i < image->height; i++) {
        if (fwrite(image->data + i * (image->width), 3, image->width, out) != image->width) {
            // fclose(out);
            // free_memory(*image);
            file_close_memory_free(out, *image);
            return WRITE_ERROR;
        }
        for (size_t j = 0; j < padding; j++){
            int8_t zero = 0;
            fwrite(&zero,1,1,out);
        }
    }
    // fclose(out);
    // free_memory(*image);
    file_close_memory_free(out, *image);
    return WRITE_OK;
}
